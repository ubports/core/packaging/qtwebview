Source: qtwebview-opensource-src
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
XSBC-Original-Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Scarlett Moore <sgmoore@kde.org>,
           Sandro Knauß <hefee@debian.org>,
           Dmitry Shachnev <mitya57@debian.org>,
           Simon Quigley <tsimonq2@debian.org>,
           Patrick Franz <deltaone@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               pkg-kde-tools,
               qtbase5-dev (>= 5.15.13+dfsg~),
               qtbase5-private-dev (>= 5.15.13+dfsg~),
               qtdeclarative5-private-dev (>= 5.15.13~),
               qtwebengine5-dev (>= 5.15.13+dfsg~),
               qtwebengine5-private-dev (>= 5.15.13+dfsg~)
Build-Depends-Indep: qdoc-qt5 (>= 5.15.13~) <!nodoc>,
                     qhelpgenerator-qt5 (>= 5.15.13~) <!nodoc>,
                     qtattributionsscanner-qt5 (>= 5.15.13~) <!nodoc>
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt/qtwebview
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt/qtwebview.git
Homepage: https://doc.qt.io/qt-5/qtwebview-index.html

Package: qtwebview5-doc
Build-Profiles: <!nodoc>
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: display web content in a QML application - Documentation
 Qt WebView provides a way to display web content in a QML application
 without necessarily including a full web browser stack by using
 native APIs where it makes sense.
 .
 This package contains the documentation for QtWebView.

Package: qtwebview5-doc-html
Build-Profiles: <!nodoc>
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: display web content in a QML application - HTML Documentation
 Qt WebView provides a way to display web content in a QML application
 without necessarily including a full web browser stack by using
 native APIs where it makes sense.
 .
 This package contains the HTML documentation for QtWebView.

Package: libqt5webview5-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libqt5webview5 (= ${binary:Version}),
         qtbase5-dev,
         qtdeclarative5-dev,
         ${misc:Depends}
Description: display web content in a QML application - Development Files
 Qt WebView provides a way to display web content in a QML application
 without necessarily including a full web browser stack by using
 native APIs where it makes sense.
 .
 This package contains the development files needed to build Qt 5 applications
 using the QtWebView library.

Package: qtwebview5-examples
Architecture: any
Multi-Arch: same
Depends: libqt5webview5 (= ${binary:Version}),
         qml-module-qtwebview,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: display web content in a QML application - Examples
 Qt WebView provides a way to display web content in a QML application
 without necessarily including a full web browser stack by using
 native APIs where it makes sense.
 .
 This package contains the examples for Qt's WebView submodule.

Package: libqt5webview5
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: display web content in a QML application - Library
 Qt WebView provides a way to display web content in a QML application
 without necessarily including a full web browser stack by using
 native APIs where it makes sense.
 .
 This package contains the QtWebView libraries.

Package: qml-module-qtwebview
Architecture: any
Multi-Arch: same
Depends: qml-module-qtwebengine, ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: display web content in a QML application
 Qt WebView provides a way to display web content in a QML application
 without necessarily including a full web browser stack by using
 native APIs where it makes sense.
 .
 This package contains the WebView QML module.
