qtwebview-opensource-src (5.15.13-1ubports1) noble; urgency=medium

  * Merge version 1.15.13-1 from Debian.

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Tue, 15 Oct 2024 00:21:59 +0700

qtwebview-opensource-src (5.15.13-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.13.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 14 Mar 2024 22:43:19 +0300

qtwebview-opensource-src (5.15.12-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.12.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 25 Dec 2023 11:15:15 +0300

qtwebview-opensource-src (5.15.10-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 08 Jul 2023 19:19:46 +0300

qtwebview-opensource-src (5.15.10-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.10.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 13 Jun 2023 23:49:44 +0300

qtwebview-opensource-src (5.15.9-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.9.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 24 Apr 2023 14:04:37 +0300

qtwebview-opensource-src (5.15.8-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 13 Jan 2023 12:02:25 +0400

qtwebview-opensource-src (5.15.8-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.8.
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 09 Jan 2023 20:13:47 +0400

qtwebview-opensource-src (5.15.7-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 17 Dec 2022 18:20:46 +0300

qtwebview-opensource-src (5.15.7-1) experimental; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + libqt5webview5: Drop versioned constraint on libqt5webview5-dev in
      Replaces.
    + libqt5webview5: Drop versioned constraint on libqt5webview5-dev in Breaks.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.15.7.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 08 Dec 2022 20:25:30 +0300

qtwebview-opensource-src (5.15.6-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 29 Sep 2022 11:55:50 +0300

qtwebview-opensource-src (5.15.6-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.6.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 14 Sep 2022 20:35:25 +0300

qtwebview-opensource-src (5.15.5-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.5.
  * Use symver directive to catch all private symbols at once.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 30 Jul 2022 21:28:24 +0300

qtwebview-opensource-src (5.15.4-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 13 Jun 2022 21:37:11 +0300

qtwebview-opensource-src (5.15.4-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.4.
  * Bump Standards-Version to 4.6.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 15 May 2022 12:48:15 +0300

qtwebview-opensource-src (5.15.3-1) experimental; urgency=medium

  * New upstream release.
  * Update debian/watch.
  * Bump Qt build-dependencies to 5.15.3.
  * Update email address for Patrick Franz.
  * Bump Standards-Version to 4.6.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 09 Mar 2022 16:27:52 +0300

qtwebview-opensource-src (5.15.2-2) unstable; urgency=medium

  * Bump Standards-Version to 4.5.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 11 Dec 2020 11:32:42 +0300

qtwebview-opensource-src (5.15.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.2.
  * Add support for nodoc build profile.
  * Build-depend only on the needed documentation tools, not on the large
    qttools5-dev-tools package.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 26 Nov 2020 20:51:12 +0300

qtwebview-opensource-src (5.15.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 28 Oct 2020 21:54:27 +0300

qtwebview-opensource-src (5.15.1-1) experimental; urgency=medium

  * Team upload.

  [ Patrick Franz ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.15.1.
  * Update debian/libqt5webview5.symbols from the current build log.
  * Use ${DEB_HOST_MULTIARCH} substitution in the install files.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 26 Sep 2020 10:54:05 -0300

qtwebview-opensource-src (5.14.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 24 Jun 2020 13:44:04 +0300

qtwebview-opensource-src (5.14.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.14.2.
  * Bump Standards-Version to 4.5.0, no changes needed.
  * Bump debhelper-compatibility to 13:
    - Switch debhelper build dependency to debhelper-compat 13.
    - Remove debian/compat.
    - Drop dh_missing override, no longer needed.
  * Add Rules-Requires-Root field.
  * Add myself to uploaders.
  * Bump years in debian/copyright.
  * Remove build paths from .prl files for reproducibility.
  * Update debian/libqt5webview5.symbols from the current build log.

 -- Patrick Franz <patfra71@gmail.com>  Mon, 11 May 2020 15:16:29 +0200

qtwebview-opensource-src (5.12.9-0~1ubports20.04) focal; urgency=medium

  * Build qtwebview with abi compat with qtwebengine 5.14

 -- Marius Gripsgard <marius@ubports.com>  Thu, 20 Jan 2022 04:47:01 +0100

qtwebview-opensource-src (5.12.9-0~ubports1) xenial; urgency=medium

  * Backport to Ubuntu Touch 16.04.

 -- Rodney Dawes <dobey@ubports.com>  Thu, 15 Jul 2021 13:26:04 -0400

qtwebview-opensource-src (5.12.8-0ubuntu1) focal; urgency=medium

  * New upstream bugfix release.
  * Bump Qt build-dependencies to 5.12.8.

 -- Dmitry Shachnev <mitya57@ubuntu.com>  Fri, 10 Apr 2020 19:29:29 +0300

qtwebview-opensource-src (5.12.5-1build1) focal; urgency=medium

  * No-change rebuild for libgcc-s1 package name change.

 -- Matthias Klose <doko@ubuntu.com>  Mon, 23 Mar 2020 07:24:05 +0100

qtwebview-opensource-src (5.12.5-1) unstable; urgency=medium

  * Make qtwebview5-examples depend on qml-module-qtwebview.
    Thanks to Mike Bird for noticing the missing dependency!
  * New upstream release.
  * Bump Qt build-dependencies to 5.12.5.
  * Bump Standards-Version to 4.4.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 21 Oct 2019 20:06:53 +0300

qtwebview-opensource-src (5.12.4-1) experimental; urgency=medium

  * New upstream release.
  * Make sure private headers are not installed.
  * Update debian/copyright.
  * Bump Qt build-dependencies to 5.12.4.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 02 Jul 2019 20:43:15 +0300

qtwebview-opensource-src (5.12.3-1) experimental; urgency=medium

  [ Scarlett Moore ]
  * Update my name/email.
  * Update packaging to use doc-base as per policy 9.10.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.12.3.
  * Use debian/clean and debian/not-installed instead of override targets.
  * Bump Standards-Version to 4.3.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 06 May 2019 21:46:36 +0300

qtwebview-opensource-src (5.12.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.2.
  * Add Qt_5.12 version tag to debian/libqt5webview5.symbols.
  * Add Build-Depends-Package field to debian/libqt5webview5.symbols.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 28 Mar 2019 23:41:30 +0300

qtwebview-opensource-src (5.11.3-2) unstable; urgency=medium

  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 26 Dec 2018 16:43:59 -0300

qtwebview-opensource-src (5.11.3-1) experimental; urgency=medium

  [ Simon Quigley ]
  * Change my email to tsimonq2@debian.org now that I am a Debian Developer.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New upstream release.
    - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Mon, 24 Dec 2018 10:44:14 -0300

qtwebview-opensource-src (5.11.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 17 Oct 2018 00:26:57 +0300

qtwebview-opensource-src (5.11.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build dependencies to 5.11.2.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Sun, 07 Oct 2018 18:52:31 -0500

qtwebview-opensource-src (5.11.1-3) unstable; urgency=medium

  * Move libqtwebview_webengine.so from libqt5webview5-dev to libqt5webview5
    (closes: #907249).
  * Make qml-module-qtwebview depend on qml-module-qtwebengine.
  * Bump Standards-Version to 4.2.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 31 Aug 2018 23:30:26 +0300

qtwebview-opensource-src (5.11.1-2) unstable; urgency=medium

  * Upload to Sid.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Wed, 25 Jul 2018 04:49:33 -0500

qtwebview-opensource-src (5.11.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-version to 4.1.5, no changes needed.
  * Bump build dependencies to 5.11.1.
  * Bump debhelper compat to 11, no changes needed.
  * Update install file for the new upstream release.
  * Update symbols from amd64 build logs.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Wed, 18 Jul 2018 04:40:12 -0500

qtwebview-opensource-src (5.10.1-2) unstable; urgency=medium

  * Release to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 07 Apr 2018 21:02:03 -0300

qtwebview-opensource-src (5.10.1-1) experimental; urgency=medium

  * New upstream release.
  * Update debian/watch for the new upstream tarballs names.
  * Bump Qt build-dependencies to 5.10.1.
  * Update Vcs fields to point to salsa.debian.org.
  * Use dh_auto_configure provided by debhelper.
  * Add Qt_5.10 symbol to debian/libqt5webview5.symbols.
  * Bump Standards-Version to 4.1.3, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 15 Mar 2018 12:48:07 +0300

qtwebview-opensource-src (5.9.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.9.2.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 27 Oct 2017 21:40:53 +0300

qtwebview-opensource-src (5.9.1-2) unstable; urgency=medium

  * Bump Standards-Version to 4.1.1, no changes needed.
  * Do not attempt to run tests, they work only with native backends.
  * Use debhelper compat 10 and dh_missing.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 04 Oct 2017 00:22:53 +0700

qtwebview-opensource-src (5.9.1-1) experimental; urgency=medium

  [ Jonathan Riddell ]
  * Initial package. (Closes: #869227)

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 21 Jul 2017 21:59:20 +0300
